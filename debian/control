Source: cantor
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Pino Toscano <pino@debian.org>,
Build-Depends: cmake (>= 3.13~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.49.0~),
               gettext,
               libanalitza-dev (>> 4:15.08),
               libglib2.0-dev,
               libkf5archive-dev (>= 5.91.0~),
               libkf5completion-dev (>= 5.91.0~),
               libkf5config-dev (>= 5.91.0~),
               libkf5coreaddons-dev (>= 5.91.0~),
               libkf5crash-dev (>= 5.91.0~),
               libkf5doctools-dev (>= 5.91.0~),
               libkf5i18n-dev (>= 5.91.0~),
               libkf5iconthemes-dev (>= 5.91.0~),
               libkf5kio-dev (>= 5.91.0~),
               libkf5newstuff-dev (>= 5.91.0~),
               libkf5parts-dev (>= 5.91.0~),
               libkf5pty-dev (>= 5.91.0~),
               libkf5syntaxhighlighting-dev (>= 5.91.0~),
               libkf5texteditor-dev (>= 5.91.0~),
               libkf5textwidgets-dev (>= 5.91.0~),
               libkf5xmlgui-dev (>= 5.91.0~),
               liblapack3,
               libluajit-5.1-dev [i386 amd64],
               libmarkdown2-dev,
               libpoppler-qt5-dev (>= 0.62.0~),
               libpython3-dev,
               libqalculate-dev (>= 3.8.0~),
               libqt5svg5-dev (>= 5.8.0~),
               libqt5xmlpatterns5-dev (>= 5.8.0~),
               libspectre-dev,
               pkg-config,
               pkg-kde-tools (>> 0.15.15),
               qtbase5-dev (>= 5.8.0~),
               qttools5-dev (>= 5.8.0~),
               qtwebengine5-dev (>= 5.8.0~),
               r-base-core,
               shared-mime-info (>= 1.3~),
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://edu.kde.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/cantor
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/cantor.git

Package: cantor
Architecture: any
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends},
Recommends: cantor-backend-qalculate, texlive-binaries, texlive-latex-base,
Suggests: cantor-backend-kalgebra,
          cantor-backend-lua [i386 amd64],
          cantor-backend-maxima,
          cantor-backend-octave,
          cantor-backend-python3,
          cantor-backend-r,
          cantor-backend-sage,
          cantor-backend-scilab [amd64 arm64 ppc64el s390x],
Description: interface for mathematical applications
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 Cantor supports various mathematical applications as backends (provided in
 external packages):
  * Maxima Computer Algebra System (cantor-backend-maxima)
  * R Project for Statistical Computing (cantor-backend-r)
  * Sage Mathematics Software (cantor-backend-sage)
  * Octave (cantor-backend-octave)
  * Python (cantor-backend-python3)
  * Scilab (cantor-backend-scilab)
  * Qalculate! (cantor-backend-qalculate)
  * Lua (cantor-backend-lua)
 .
 This package is part of the KDE education module.

Package: libcantorlibs28abi1
X-Debian-ABI: 1
X-CMake-Target: cantorlibs
Architecture: any
Section: libs
Depends: libcantorlibs-data (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: interface for mathematical applications - shared library
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the shared library for Cantor.
 .
 This package is part of the KDE education module.

Package: libcantorlibs-data
Architecture: all
Section: libs
Depends: ${misc:Depends},
Replaces: cantor (<< 4:20.12.0-2~), libcantorlibs28 (<< 4:22.04.1-1~),
Breaks: cantor (<< 4:20.12.0-2~), libcantorlibs28 (<< 4:22.04.1-1~),
Description: interface for mathematical applications - data for shared library
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the data for the shared library for Cantor.
 .
 This package is part of the KDE education module.

Package: libcantor-dev
Architecture: any
Section: libdevel
Depends: libcantorlibs28abi1 (=  ${binary:Version}), ${misc:Depends},
Description: interface for mathematical applications - development files
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the development headers for Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-kalgebra
Architecture: any
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: KAlgebra backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using KAlgebra
 (https://edu.kde.org/kalgebra) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-lua
Architecture: amd64 i386
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: Lua backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Lua language
 (https://www.lua.org) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-maxima
Architecture: any
Section: math
Depends: maxima, ${misc:Depends}, ${shlibs:Depends},
Description: Maxima backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Maxima Computer Algebra System
 (http://maxima.sourceforge.net) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-octave
Architecture: any
Section: math
Depends: octave, ${misc:Depends}, ${shlibs:Depends},
Description: Octave backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the GNU Octave language for
 numerical computations (https://www.octave.org/) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-python3
Architecture: any
Section: math
Depends: python3, ${misc:Depends}, ${shlibs:Depends},
Breaks: libcantor-pythonbackend (<< 4:20.04.2~),
Replaces: libcantor-pythonbackend (<< 4:20.04.2~),
Description: Python3 backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Python3 language
 (https://www.python.org/) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-qalculate
Architecture: any
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: Qalculate! backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Qalculate! calculator
 (https://qalculate.github.io/) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-r
Architecture: any
Section: math
Depends: r-base-core, ${misc:Depends}, ${shlibs:Depends},
Suggests: r-base-html,
Description: R backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the R Project for Statistical
 Computing (https://www.r-project.org) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-sage
Architecture: any
Section: math
Depends: ${misc:Depends}, ${shlibs:Depends},
Suggests: sagemath,
Description: Sage backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Sage Mathematics Software
 (https://www.sagemath.org) in Cantor.
 .
 This package is part of the KDE education module.

Package: cantor-backend-scilab
Architecture: amd64 arm64 ppc64el s390x
Section: math
Depends: scilab-cli, ${misc:Depends}, ${shlibs:Depends},
Description: Scilab backend for Cantor
 Cantor is an application to allow you to you use your favorite mathematical
 applications from within an elegant worksheet interface. It provides dialogs
 to assist with common tasks and allows you to share your worksheets
 with others.
 .
 This package provides the backend for using the Scilab scientific software
 package for numerical computations (https://www.scilab.org) in Cantor.
 .
 This package is part of the KDE education module.
